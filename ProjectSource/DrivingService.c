/****************************************************************************
 Module
   DrivingService.c

 Revision
   1.0.1

 Description
   This is the first service for the Test Harness under the
   Gen2 Events and Services Framework.
/*----------------------------- Include Files -----------------------------*/
// This module
#include "../ProjectHeaders/DrivingService.h"

// Hardware
#include <xc.h>

// Event & Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_Port.h"
#include "terminal.h"
#include "dbprintf.h"
#include "PIC32_AD_Lib.h"

/*----------------------------- Module Defines ----------------------------*/
#define ENTER_POST     ((MyPriority<<3)|0)
#define ENTER_RUN      ((MyPriority<<3)|1)
#define ENTER_TIMEOUT  ((MyPriority<<3)|2)
#define DELAY 100
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

static void InitLED(void);
static void BlinkLED(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static uint32_t PrevShotTime;
static uint32_t PrevTargetShotTime;

bool InitDrivingService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;

  clrScrn();
  
  // initialize LED drive for testing/debug output
   InitLED();
   ADC_ConfigAutoScan(BIT12HI, 1);
   
   PrevShotTime = ES_Timer_GetTime();
   PrevTargetShotTime = PrevShotTime;

  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool PostDrivingService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

ES_Event_t RunDrivingService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  uint32_t CurrentShotTime = ES_Timer_GetTime();
  
  switch (ThisEvent.EventType)
  {
    case ES_INIT:
    {
      ES_Timer_InitTimer(SERVICE0_TIMER, DELAY);   
      puts("Service 00:");
      DB_printf("\rES_INIT received in Service %d\r\n", MyPriority);
    }
    break;
    case ES_NEW_KEY:   // announce
    {
      BlinkLED();
      printf("ES_NEW_KEY received with -> %c <- in Service 0\r\n",
          (char)ThisEvent.EventParam);
    }
    case ES_TIMEOUT:
    {
        ES_Timer_InitTimer(SERVICE0_TIMER, DELAY);
        uint32_t adcResults;
        ADC_MultiRead(&adcResults);
        printf("STATE OF PIN: %u\r\n", adcResults);
        printf("Time: %u\r\n", CurrentShotTime - PrevTargetShotTime);
        printf("State: %u\r\n", PORTBbits.RB9);
        if (PORTBbits.RB9 == 0 && CurrentShotTime - PrevTargetShotTime > 3500) {
            LATBbits.LATB9 = 1;
        }
    }
    break;
    case ES_TARGET_SHOT:
    {
        if (PORTBbits.RB9 == 1 && CurrentShotTime - PrevShotTime > 1000) {
            printf("Target shot!");
            printf("%d", CurrentShotTime);
            PrevShotTime = CurrentShotTime;
            PrevTargetShotTime = CurrentShotTime;
            LATBbits.LATB9 = 0;
        } else {
//            printf("Target shot but delay not long enough");
        }
    }
    break;
    default:
    {}
     break;
  }

  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/
#define LED LATBbits.LATB9
static void InitLED(void)
{
  LED = 1;
  TRISBbits.TRISB9 = 0;
//  ANSELBbits.ANSB12 = 1;
  TRISBbits.TRISB12 = 1;
}

static void BlinkLED(void)
{
  // toggle state of LED
  LED = ~LED;
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

